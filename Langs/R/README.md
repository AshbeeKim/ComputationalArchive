<!--header-->
<img src="https://capsule-render.vercel.app/api?type=waving&color=0:195daf,100:aaaab4&height=250&section=header&text=Langs&fontSize=80&fontAlign=20&fontAlignY=30&desc=Programing%20Language%20%3A%20%20R&descSize=20&descAlign=80&descAlignY=60&&fontColor=fff" />

<!--contents-->
<h1><b>R</b></h1>
<h2><span>Steps</span></h2>
<div>
    <li><a href="">Introduction</a></li>
    <li>Programming in R</li>
    <li>Data Structures</li>
    <li>Analyzing Data with R</li>
    <li>Visualization</li>
</div>

<h2><span>Tip</span></h2>
<div>
    <h3>with colab</h3>
    <div>
        <li></li>
    </div>
</div>

<!--footer-->
<img src="https://capsule-render.vercel.app/api?type=waving&color=0:195daf,100:aaaab4&height=250&section=footer&text=Thank%20You&fontSize=50&fontAlignY=70&fontColor=fff"/>
